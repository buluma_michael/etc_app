cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-camera.Camera",
    "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "Camera"
    ]
  },
  {
    "id": "cordova-plugin-camera.CameraPopoverOptions",
    "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "CameraPopoverOptions"
    ]
  },
  {
    "id": "cordova-plugin-camera.camera",
    "file": "plugins/cordova-plugin-camera/www/Camera.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "navigator.camera"
    ]
  },
  {
    "id": "cordova-plugin-camera.CameraPopoverHandle",
    "file": "plugins/cordova-plugin-camera/www/CameraPopoverHandle.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "CameraPopoverHandle"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-dialogs.notification",
    "file": "plugins/cordova-plugin-dialogs/www/notification.js",
    "pluginId": "cordova-plugin-dialogs",
    "merges": [
      "navigator.notification"
    ]
  },
  {
    "id": "cordova-plugin-dialogs.notification_android",
    "file": "plugins/cordova-plugin-dialogs/www/android/notification.js",
    "pluginId": "cordova-plugin-dialogs",
    "merges": [
      "navigator.notification"
    ]
  },
  {
    "id": "cordova-plugin-locationservices.Coordinates",
    "file": "plugins/cordova-plugin-locationservices/www/Coordinates.js",
    "pluginId": "cordova-plugin-locationservices",
    "clobbers": [
      "cordova.plugins.locationServices.Coordinates",
      "plugin.locationServices.Coordinates"
    ]
  },
  {
    "id": "cordova-plugin-locationservices.PositionError",
    "file": "plugins/cordova-plugin-locationservices/www/PositionError.js",
    "pluginId": "cordova-plugin-locationservices",
    "clobbers": [
      "cordova.plugins.locationServices.PositionError",
      "plugin.locationServices.PositionError"
    ]
  },
  {
    "id": "cordova-plugin-locationservices.Position",
    "file": "plugins/cordova-plugin-locationservices/www/Position.js",
    "pluginId": "cordova-plugin-locationservices",
    "clobbers": [
      "cordova.plugins.locationServices.PositionError",
      "plugin.locationServices.PositionError"
    ]
  },
  {
    "id": "cordova-plugin-locationservices.LocationServices",
    "file": "plugins/cordova-plugin-locationservices/www/LocationServices.js",
    "pluginId": "cordova-plugin-locationservices",
    "clobbers": [
      "cordova.plugins.locationServices.geolocation",
      "plugin.locationServices.geolocation"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-camera": "1.2.0",
  "cordova-plugin-device": "1.1.5",
  "cordova-plugin-dialogs": "1.3.2",
  "cordova-plugin-locationservices": "2.1.0",
  "cordova-plugin-whitelist": "1.3.2"
};
// BOTTOM OF METADATA
});