$(document).ready(function(){
    console.log('lets begin');
});

// Wait for Cordova to load
// document.addEventListener('deviceready', onDeviceReady, false);

function onLoad() {
        // document.addEventListener("deviceready", onDeviceReady, false);
        console.log('loaded');
    }

    // PhoneGap is loaded and it is now safe to make calls PhoneGap methods
    //
    function onDeviceReady() {
        // Now safe to use the PhoneGap API
        console.log('ready');
    }

$(function() 
{ //shorthand document.ready function
document_ready = true;

stored_view = localStorage.getItem("view");

if(!stored_view)
{
    //home
    // change_view("home");
    console.log('home');
}
else if(stored_view != view)
{
    //go to stored view
    // change_view(stored_view);
    console.log('stored_view');
}

});

function device_is_ready()
{
phonegap_ready = true;

if(navigator.network.connection.type == Connection.NONE ||  navigator.network.connection.type == Connection.UNKNOWN)
{
    online = false;
    alert("offline");
}
else
{
    online = true;
    alert("online");
}
}

document.addEventListener("deviceready", device_is_ready, false);

// // Cordova is ready
// function onDeviceReady() {
//   // var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});
//   var db = window.openDatabase("my.db", '1.0', "ETC Database",50*1024*1024);
//   console.log(db);

//   db.transaction(function(tx) {
//     tx.executeSql('DROP TABLE IF EXISTS test_table');
//     tx.executeSql('CREATE TABLE IF NOT EXISTS test_table (id integer primary key, data text, data_num integer)');

//     // demonstrate PRAGMA:
//     db.executeSql("pragma table_info (test_table);", [], function(res) {
//       console.log("PRAGMA res: " + JSON.stringify(res));
//     });

//     tx.executeSql("INSERT INTO test_table (data, data_num) VALUES (?,?)", ["test", 100], function(tx, res) {
//       console.log("insertId: " + res.insertId + " -- probably 1");
//       console.log("rowsAffected: " + res.rowsAffected + " -- should be 1");

//       db.transaction(function(tx) {
//         tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
//           console.log("res.rows.length: " + res.rows.length + " -- should be 1");
//           console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
//         });
//       });

//     }, function(e) {
//       console.log("ERROR: " + e.message);
//     });
//   });
// }