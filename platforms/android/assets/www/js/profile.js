//outlets
function fetchMyStores() {  
    var q = "SELECT * FROM stores ORDER BY name ASC";
    db.transaction(function (t) {
        t.executeSql(q, null, function (t, data) {            
            var sl ='';
            for (var i =0;i<data.rows.length;i++) {
                sl += '<a href="storemenu.html?store_id='+data.rows.item(i).id+'&store_server_id='+data.rows.item(i).server_id+'&store_name='+data.rows.item(i).name+'" class="list-group-item list-group-item-info">';
                sl += '<span class="glyphicon glyphicon-home big-icon2 pull-left"></span><h4>'+data.rows.item(i).name+'</h4>';
                sl += '<p><span class="glyphicon glyphicon-map-marker"> </span>  '+data.rows.item(i).region+ '</p>';
                sl += '</a>';
            }
            //sl += '</ul>';
            $('article#storelist .dataList').html(sl);
        });
    });
}


$(document).ready(function(){
    // let's hide the stores link
    var homeinfo = '';
    // var homeinfo = '<div class="app-info thumbnail">';
    // homeinfo +='<a href="#" class="home-info label-warning" data-toggle="modal" data-target="#userModal">';
    // homeinfo +='  <div class="valign"><span class="icon icon-user big-icon"></span> <h4> '+fullname+ '</h4></div>';
    // homeinfo +='</a>';
    // homeinfo +='<a href="#" class="home-info label-info">';
    // homeinfo +='  <div class="valign"><span class="icon icon-calendar big-icon"></span><h4> '+getTodaysDate()+' </h4></div>';
    // homeinfo +='</a>';
    // homeinfo +='</div>';

    // user is not known
    if (userobj.is_promoter == 'undefined') {
        //$('a#syncpagelink').addClass('hidden'); // hide the sync
    }
    homeinfo +='</div>';

    //outlets here
    homeinfo +='</div>';
    // homeinfo +='<hr/>';
    homeinfo +='<h4>Profile Details</h4>';
    homeinfo +='<p>Username: ' +username+'</p>';
    homeinfo +='<p>Full names: ' +fullname+'</p>';
    homeinfo +='<p>Email: '+useremail+'</p>';
    homeinfo +='<p>Last Login: '+userlastlogin+'</p>';
    homeinfo +='</div>';
    // $('#apphome').append(fetchMyStores());
    // countStores();
    // console.log(fetchMyStores());
    homeinfo +='<hr/>';
    homeinfo +='<h4>My Outlets</h4>';
    $('#apphome').append(homeinfo);
    fetchMyStores();
    // ;

});

$(document).ready(function() {
    // var stores = fetchMyStores();
    console.log(userobj);
    var modaluserinfo = '<ul class="list-group">';
    modaluserinfo += '<li class="list-group-item">';
    modaluserinfo += ' <h4><strong>User ID: </strong></h4>';
    modaluserinfo += '<p>'+userobj.userid+'</p>';
    modaluserinfo += '</li>';
    modaluserinfo += '<li class="list-group-item">';
    modaluserinfo += '  <h4><strong>Username: </strong><h4>';
    modaluserinfo += '<p>'+username+'</p>';
    modaluserinfo += '</li>';
    modaluserinfo += '<li class="list-group-item">';
    modaluserinfo += '  <h4><strong>Full Name: </strong><h4>';
    modaluserinfo += '<p>'+fullname+'</p>';
    modaluserinfo += '</li>';
    modaluserinfo += '<li class="list-group-item">';
    modaluserinfo += '  <h4><strong>User Email: </strong><h4>';
    modaluserinfo += '<p>'+useremail+'</p>';
    modaluserinfo += '</li>';
    modaluserinfo += '<li class="list-group-item">';
    modaluserinfo += '  <h4><strong>Last Login: </strong><h4>';
    modaluserinfo += '<p>'+userlastlogin+'</p>';
    modaluserinfo += '</li>';
    modaluserinfo += '</ul>';

    $('button#btnlogout').on('click', function(){
        logOutUser();
    });

  $('#userModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body').html(modaluserinfo);
    })

});

