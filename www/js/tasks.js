
var week = moment().week();
var month = thisMonth();
var year = thisYear();
var submitter = username;
var iNum = 0;

const now = new Date();
console.log('time is ' + now);

function addObj() { 
  iNum ++;
  // compacted to prevent string errors caused by white spaces
  var imei = '<div class="form-group list-group-item" id="iNum'+iNum+'">';
  imei += '  <div class="col-xs-9">';
  imei += '     <label>IMEI</label><input name="imeinumber[]" id="'+iNum+'" class="form-control" type="text">';
  imei += '  </div>';
  imei += '  <div class="col-xs-3">';
  imei += '     <button type="button" class="btn btn-success scanbtn" id="scanIMEI'+iNum+'" data-scan="'+iNum+'"><span class="glyphicon glyphicon-screenshot"></span></button>';
  imei += '     <button type="button" class="btn btn-danger deletebtn" id="deleteIMEI'+iNum+'" data-delete="'+iNum+'"><span class="glyphicon glyphicon-remove-sign"></span></button>';
  imei += '  </div>';
  imei += '</div>';
  $('article#inputform .imeiList').append(imei);
  $('#saveholder').removeClass('hidden');
 
}

function removeObj(num) {
  $('#scanedlist #'+num).remove();
}

function fetchObj() {  
  // var q = "SELECT * FROM tasks WHERE week = '"+week+"' AND month = '"+month+"' AND year = '"+year+"'";
  var q = "SELECT * FROM tasks";
  
  db.transaction(function (t) {
    t.executeSql(q, null, function (t, data) { 
      var found = data.rows.length; 
      // console.log(found);
      // console.log(data); 
      var pl ='';
      if (found >= 1) {
          pl +='<ul class="list-group">';
          for (var i =0;i<data.rows.length;i++) {        
            // console.log(data.rows.item(i));
            var objs = JSON.parse(data.rows.item(i).task_id);
            // console.log(objs);
            //for (var x = 0; x < )
            $.each(objs.objectives, function(index, element) {
              // console.log(element);
              // console.log(index);
              pl += '<li class="list-group-item"><a href="#" class="btn btn-primary btn-edit-stock" data-toggle="modal" data-target="#form-modal">';
              pl += element;
              pl += '</li></a>';
            }); 
            
          }
          pl += '</ul>';
      }
      else{
         pl += '<p>No tasks for this week found</p>';
      }
      
	    $('article#imeilistAll .imeiListdata').html(pl);
    });
  });
}

$(document).on('click','.btn-edit-stock',function(){
      $input = $(this);
      // console.log(data);
      bindToForm($input.data('object'));
  });


// bindToForm
function bindToForm(objectives){
  // var q = "SELECT * FROM tasks";
  //      db.transaction(function (t) {
  //         t.executeSql(q, null, function (t, data) { 
  //          var found = data.rows.length; 
  //          console.log(found);

  //   });
  // });
  // accepts a valid JSON-rep object
  console.log(objectives);
  // Trigger open the modal and populate the form
  $('#selectbrands').hide();
  $('#myModalLabel').html('Editing '+data.brand);
  $('#formskuinput #brand_stock_id').val(data.id);
  $('#formskuinput #brandname').val(data.brand);
  $('#formskuinput #brandcode').val(data.brandcode);
  $('#formskuinput #brand_stock, #brand_newstock').val(data.currentstock);
  //$('#formskuinput #brand_newstock').val(data.currentstock);
  if (data.sale == '' || data.sale == null){
    data.sale = 0;
  }
  $('#formskuinput #brand_sale').val(data.sale);
  $('#formskuinput #brand_order').val(data.orderplaced);
  $('#formskuinput #brand_delivery').val(data.expected_delivery);
  $('#formskuinput #lpo_number').val(data.lpo_number);
  $('#form-modal').modal('show');

}


function insertObj() {
  
  var items = {};
  var objectives = [];

  $('#scanedlist .list-group-item').each(function() {
    var obj = $(this).children('input').val();
    // console.log(obj);
    objectives.push(obj);
  });

  items['objectives'] = objectives;
  var itemsdata = JSON.stringify(items);
  console.log(objectives);
  console.log(items);
  console.log(itemsdata);

  db.transaction(function(st) { 
      //st.executeSql('DROP TABLE imei');
      st.executeSql('CREATE TABLE IF NOT EXISTS tasks (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, task_id INTEGER, user VARCHAR, task VARCHAR, description VARCHAR, deadline VARCHAR,priority VARCHAR)');
      // st.executeSql("INSERT INTO tasks(task_id,user,task,description,deadline,priority) values ('"+itemsdata+"','"+submitter+"','"+week+"','"+month+"','"+year+"','"+now+"')",null, alertSuccess);
      st.executeSql("INSERT INTO tasks(task_id,user,task,description,deadline,priority) values ('"+itemsdata+"','"+submitter+"','"+week+"','"+month+"','"+year+"','"+now+"')",null, alertSuccess);
  },onError,function(){
    document.location.reload('true');
  });
}

function insertObj2() {
  var coords = userlocation;
  var brandcode = document.getElementById("brands").value;
  var brand = $("#brands option:selected").text();
  var expiry_date = document.getElementById("date_expiry").value;
  var issue_type = document.getElementById("issue_type").value;
  var rateofsale = document.getElementById("issue_rateofsale").value;
  var remarks = document.getElementById("issue_remarks").value;
  var submitter = username;
  var store = storename;

  db.transaction(function(st) { 
    // st.executeSql('DROP TABLE quality_issues');
    // st.executeSql('CREATE TABLE IF NOT EXISTS quality_issues (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, coords VARCHAR,brand VARCHAR,brandcode VARCHAR,issue_type VARCHAR,rateofsale VARCHAR, expiry_date VARCHAR, submitter VARCHAR,store VARCHAR,store_id INTEGER, store_server_id INTEGER, remarks VARCHAR, created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,last_sync TEXT DEFAULT "none")');
     st.executeSql('CREATE TABLE IF NOT EXISTS quality_issues (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, coords VARCHAR,brand VARCHAR,brandcode VARCHAR,issue_type VARCHAR,rateofsale VARCHAR, expiry_date VARCHAR, submitter VARCHAR,store VARCHAR,store_id INTEGER, store_server_id INTEGER, remarks VARCHAR, created_on VARCHAR,last_sync TEXT DEFAULT "none")');
    st.executeSql("INSERT INTO quality_issues(coords,brand,brandcode,expiry_date,issue_type,rateofsale,submitter,store,store_id,store_server_id,remarks, created_on) values (?,?,?,?,?,?,?,?,?,?,?,?)",[coords,brand,brandcode,expiry_date,issue_type,rateofsale,submitter,store,itemid,store_server_id,remarks, now], alertSuccess);
  },function(st,error){
    console.log(error.message)
  },onReadyTransaction);
}

$(document).ready(function() {
  fetchObj();
  $('button#btnaddObj').on('click', function(){
     //addIMEI();
    showInput();
  });

  $('#objSubmit').on('click',function(){
    insertObj();
  })
  $('#btnsyncObj').on('click',function(){
    syncTasks();
  })
  $(document).on('click','.deletebtn', function(event){
    //console.log(this.id);
    var todelete = $(this).attr('data-delete');
    removeObj(todelete);
  }); 
  if ($('#scanedlist div.list-group-item').length > 0){
    $('#saveholder').show();
  }
  else {
    $('#saveholder').hide();
  }
  
});

var i = 0;
function showInput(){ 
  i ++;
  var html = '<div class="list-group-item" id="'+i+'">';
  html +='<button type="button" class="btn btn-danger pull-right deletebtn" id="deleteObj'+i+'" data-delete="'+i+'">';
  html +='<span class="glyphicon glyphicon-remove-sign"></span>';
  html +='</button>';
  html +='<input type="text" id="objective'+i+'" class="form-control" placeholder="task '+i+'"/>';
  html +='<div>';
  $('#scanedlist').append(html);
  $('#saveholder').show();
}
function syncTasks(){
  // fetch the items from database and sync to server
  var q = "SELECT * FROM weekly_objectives WHERE last_sync = 'none'";
  
  db.transaction(function (t) {
    t.executeSql(q, null, function (t, data) { 
      var found = data.rows.length; 
      var msg ='';
      var items = [];
      var jsonitems = {};
      var jsondata;
      if (found >= 1) {
          for (var i =0;i<found;i++) {        
            var obj = data.rows.item(i);
            //console.log(obj);
            items.push(obj);
          }
          jsonitems['objectives'] = items;
          jsondata = JSON.stringify(jsonitems);
          //console.log(items);
          //console.log(jsondata);
          // ajax send this
          ajaxUpload(jsondata);

      }
      else{
         msg += '<p>You do not have any unsynced weekly objectives</p>';
      }
      
      $('.syncmessage').html(msg);
    });
  });
}
function ajaxUpload(jsondata){
   $.ajax({
        url : ServerURI+'/api/postdata.php?info=weekly_objectives&username='+username,
        type : 'POST',
        data : jsondata,
        beforeSend : function(xhr){
            console.log('Start');
            $('.ajax_request').removeClass('hide');
        },
        error : function(xhr, status, error){
            console.log(xhr.responseText+ ' | ' +status+ '|' +error);
            $('.syncmessage').html('There was an error uploading objectives');
        },
        complete : function(xhr, status){
            console.log('End');
            $('.ajax_request').addClass('hide');
        },
        success : function(result, status, xhr){
            $('.syncmessage').html('success');
            //console.log(result);
            if (result instanceof Object){
              //console.log(result);
              $('.syncmessage').html(result.message);
              if(result.status == 'OK'){
                // update the syncdate of synced items
                updateSyncDates(result.synctime,result.items_synced)
              }
            }
            // we did not get a json response
            else {
              $('.syncmessage').html('The response message could not be understood');
            }           
            
        }
    
    });

}
function updateSyncDates(lastsync,itemids){
  // check the length of itemids
  if (itemids.length){
      db.transaction(function(st) { 
          for (var i = itemids.length - 1; i >= 0; i--) {
            console.log(itemids[i]);
            console.log(lastsync);
            st.executeSql("UPDATE weekly_objectives SET last_sync = '"+lastsync+"' WHERE id = '"+itemids[i]+"'",null, alertSuccess);
          };
      },onError,function(){
        //document.location.reload('true');
      });
  }
  
}

