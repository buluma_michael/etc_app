$(document).ready(function(){
  fetchObjectives();
});
// grab url params
// Getting URL var by its name
var itemtype = $.getUrlVar('type');
var itemid = $.getUrlVar('store_id');
var storename = decodeURI($.getUrlVar('store_name'));
var store_server_id = $.getUrlVar('store_server_id');

const now = new Date();
console.log('time is ' + now);

function fetchObjectives(){
    var q = "SELECT * FROM perfect_checklist WHERE store_id = ? ORDER BY created DESC LIMIT 10"; 
    db.transaction(function (t) {
        t.executeSql(q, [itemid], function (t, data) {     
          var pl ='';
          for (var i =0;i<data.rows.length;i++) {
            pl += '<a href="#" class="list-group-item">';
            pl += '<span class="badge">'+data.rows.item(i).created+'</span>';
            pl += '<h4 class="list-group-item-heading">'+data.rows.item(i).shop_mml+'</h4>';
            pl += '<p><strong>Sku Available:</strong> '+data.rows.item(i).sku_available+'</p>';
          
            // if (data.rows.item(i).objective_achieved == 'No'){
            //     pl += '<p><strong>Challenge Faced:</strong> '+data.rows.item(i).challenge+'</p>';
            // }  
            
            // pl += '<p><strong>Next Plan:</strong> '+data.rows.item(i).next_plan+'</p>';
            // pl += '</a>';
          }
              $('article#objectivelist .dataList').html(pl);
        });
    });
    
}
function saveObjective() {
    var coords = userlocation;
    var inputdate = getTodaysDate();
    var shop_mml = document.getElementById("brands").value;
    var sku_available = document.getElementById("sku_available").value;
    var merchandising = document.getElementById("merchandising").value;
    var shelf_quantity = document.getElementById("shelf_quantity").value;
    var right_prices = document.getElementById("right_prices").value;
    var visible_tags = document.getElementById("visible_tags").value;
    var traffic_area = document.getElementById("traffic_area").value;
    var eye_level = document.getElementById("eye_level").value;
    var posm = document.getElementById("posm").value;
    var priority_gondolas = document.getElementById("priority_gondolas").value;
    var retailer_informed = document.getElementById("retailer_informed").value;
    var promotion_visible = document.getElementById("promotion_visible").value;
    var wobbler = document.getElementById("wobbler").value;
    var posters_place = document.getElementById("posters_place").value;
    var gondolas_installed = document.getElementById("gondolas_installed").value;
    var submitter = username;
    var store = storename;

    db.transaction(function(st) { 
        //st.executeSql('DROP TABLE objectives');
        st.executeSql('CREATE TABLE IF NOT EXISTS perfect_checklist (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,inputdate VARCHAR, coords VARCHAR,shop_mml VARCHAR, sku_available INTEGER, merchandising INTEGER, shelf_quantity INTEGER,right_prices INTEGER,visible_tags INTEGER,traffic_area INTEGER,eye_level INTEGER,posm INTEGER,priority_gondolas INTEGER,retailer_informed INTEGER,promotion_visible INTEGER,wobbler INTEGER,posters_place INTEGER,gondolas_installed INTEGER, submitter VARCHAR,store VARCHAR,store_id INTEGER, store_server_id INTEGER,modified TEXT, created VARCHAR NOT NULL,last_sync TEXT DEFAULT "none")');
        st.executeSql("INSERT INTO perfect_checklist(inputdate,coords,shop_mml,sku_available,merchandising,shelf_quantity,right_prices,visible_tags,traffic_area,eye_level,posm,priority_gondolas,retailer_informed,promotion_visible,wobbler,posters_place,gondolas_installed,submitter,store,store_id,store_server_id,created) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            [inputdate,coords,shop_mml,sku_available,merchandising,shelf_quantity,right_prices,visible_tags,traffic_area,eye_level,posm,priority_gondolas,retailer_informed,promotion_visible,wobbler,posters_place,gondolas_installed,submitter,store,itemid,store_server_id,now], alertSuccess);
    },function(error){
        console.log(error.message)
    },onReadyTransaction);
}

var backLink = '<h5><a href="storemenu.html?store_id='+itemid+'&store_server_id='+store_server_id+'&store_name='+storename+'" class="button">';
backLink += '<span class="glyphicon glyphicon-arrow-left"> '+storename+'</a></h5>';

var newsku = '<h4 class="pull-left"> Perfect Store Checklist</h4>';
newsku += '<button type="button" class="btn btn-warning pull-right" data-toggle="modal" data-target="#form-modal">Add</button>';

// var lenovomodels = '<label>Product</label>';
  var lenovomodels = '<select class="form-control" id="brands">';
  //lenovomodels += main_products;
  lenovomodels += '</select>';

$(document).ready(function() {
    $('.navbar-header').append(backLink);
    $('#newsku').append(newsku);

    $('#selectvocbrands').html(lenovomodels);
    fetchProductsForSelect(function(options){
        $('#brands').append(options);
      })

    $('form#objectivesinput').on('submit', function(e){
        e.preventDefault();
        if (formValidated(this)){
            saveObjective();
            $('#objectivesinput input)').val('');      
        }           
    });
    $('#form-modal').on('hidden.bs.modal', function (e) {
        fetchObjectives();
    })
});